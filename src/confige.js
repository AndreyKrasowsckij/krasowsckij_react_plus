export const serverAttr = {
  registration: 'http://localhost:3000/registration',
  login: 'http://localhost:3000/login',
  authentication: 'http://localhost:3000/authentication',
  users: 'http://localhost:3000/users',
  image: 'http://localhost:3000/image',
  info: 'http://localhost:3000/info',
  newslist: 'http://localhost:3000/newslist',
  news:  'http://localhost:3000/news'
}

export const localStoragePath = {
  jwt: 'jwt',
  username: 'username'
}

export const pageConf = {
  count: 6
}
