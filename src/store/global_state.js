import LocalStorage from 'localStorage'
import { localStoragePath } from './../confige'
import { browserHistory } from 'react-router'

export const AUTHORIZATION_CHANGE = 'AUTHORIZATION_CHANGE'

export function changeAuthorization (authStatus, id, jwt) {
  return {
    type: AUTHORIZATION_CHANGE,
    auth_status: authStatus,
    id: id,
    jwt: jwt
  }
}

export function logOut () {
  return (dispatch, getState) => {
    dispatch(changeAuthorization(false, -1, ''))
    LocalStorage.setItem(localStoragePath.jwt, '')
    browserHistory.push('/')
  }
}

const initialState = { auth_status: false, id: -1, jwt: '' }
export default function globalStateReducer (state = initialState, action) {
  switch (action.type) {
    case AUTHORIZATION_CHANGE:
      return { ...state, auth_status: action.auth_status, id: action.id, jwt: action.jwt }

    default:
      return state
  }
}
