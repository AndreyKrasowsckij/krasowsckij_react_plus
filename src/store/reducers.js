import { combineReducers } from 'redux'
import locationReducer from './location'
import globalStateReducer from './global_state'
import newsList from './../routes/NewsList/modules/newslist_reducer'

export const makeRootReducer = (asyncReducers) => {
  return combineReducers({
    location: locationReducer,
    global_state: globalStateReducer,
    news_list: newsList,
    ...asyncReducers
  })
}

export const injectReducer = (store, { key, reducer }) => {
  if (Object.hasOwnProperty.call(store.asyncReducers, key)) return

  store.asyncReducers[key] = reducer
  store.replaceReducer(makeRootReducer(store.asyncReducers))
}

export default makeRootReducer
