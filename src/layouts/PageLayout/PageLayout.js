import React from 'react'
import { IndexLink, Link } from 'react-router'
import PropTypes from 'prop-types'
import './PageLayout.scss'

class PageLayout extends React.Component {
  render () {
    const { authStatus } = this.props
    const { logOut } = this.props

    return (
      <div className='container text-center'>
        {authStatus ? <h1>Login</h1> : <h1>Logout</h1> }
        <IndexLink to='/' activeClassName='page-layout__nav-item--active'>News</IndexLink>
        {' · '}
        {authStatus ? <Link to='/users' activeClassName='page-layout__nav-item--active'>User</Link> :
          <Link to='/registration' activeClassName='page-layout__nav-item--active'>Registration</Link>}
        {' · '}
        {authStatus ? <button className='btn btn-primary' onClick={logOut}>Logout</button> :
          <Link to='/login' activeClassName='page-layout__nav-item--active'>Login</Link>}
      </div>
    )
  }
}
PageLayout.propTypes = {
  authStatus: PropTypes.bool.isRequired,
  logOut: PropTypes.func.isRequired
}

export default PageLayout
