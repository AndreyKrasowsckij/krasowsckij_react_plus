import RegistrationRoute from './Registration'
import LoginRoute from './Login'
import UsersRoute from './Users'
import NewsRoute from './News'
import GuestRoute from './Guest'

export const createRoutes = (store) => (
  {
    path: '/',
    indexRoute: NewsRoute(store),
    childRoutes: [
      RegistrationRoute(store),
      LoginRoute(store),
      UsersRoute(store),
      GuestRoute(store)
    ]
  }
)

export default createRoutes
