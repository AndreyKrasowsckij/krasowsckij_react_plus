import { connect } from 'react-redux'
import { sendData, changeUsername, changePassword } from '../modules/registration_creators'
import Registration from '../components/Registration'

const mapDispatchToProps = {
  sendData,
  changeUsername,
  changePassword
}

const mapStateToProps = (state) => ({
  regStatus : state.registration.reg_status,
  regFetching: state.registration.reg_fetching,
  username: state.registration.username,
  password: state.registration.password,
  answer: state.registration.answer,
  authStatus: state.global_state.auth_status
})

export default connect(mapStateToProps, mapDispatchToProps)(Registration)
