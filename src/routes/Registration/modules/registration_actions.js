export const Actions = {
  SEND_REGDATA_REQUEST: 'SEND_REGDATA_REQUEST',
  SEND_REGDATA_SUCESS: 'SEND_REGDATA_SUCESS',
  SEND_REGDATA_ERROR: 'SEND_REGDATA_ERROR',
  REG_CHANGE_PASSWORD: 'REG_CHANGE_PASSWORD',
  REG_CHANGE_USERNAME: 'REG_CHANGE_USERNAME'
}
