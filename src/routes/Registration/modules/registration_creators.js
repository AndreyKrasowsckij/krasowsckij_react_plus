import { serverAttr, localStoragePath } from './../../../confige'
import { Actions } from './registration_actions'
import { Answer } from './registration_answer'
import LocalStorage from 'localStorage'
import { browserHistory } from 'react-router'
import Axios from 'axios'
import { changeAuthorization } from './../../../store/global_state'

export function sendData () {
  return (dispatch, getState) => {
    dispatch({
      type: Actions.SEND_REGDATA_REQUEST
    })
    Axios({
      method: 'post',
      url: serverAttr.registration,
      data: {
        username: getState().registration.username,
        password: getState().registration.password
      }
    })
      .then(response => {
        if (response.data.type === Answer.REGDATA_CORRECT) {
          dispatch({
            type: Actions.SEND_REGDATA_SUCESS,
            answer: Answer.REGDATA_CORRECT
          })
          LocalStorage.setItem(localStoragePath.jwt, response.data.jwt)
          dispatch(changeAuthorization(true, response.data.id, response.data.jwt))
          browserHistory.push('/')
        }
        if (response.data.type === Answer.REGDATA_LOGIN_EXIST) {
          dispatch({
            type: Actions.SEND_REGDATA_SUCESS,
            answer: Answer.REGDATA_LOGIN_EXIST
          })
        }
        if (response.data.type === Answer.REGDATA_DB_ERROR) {
          dispatch({
            type: Actions.SEND_REGDATA_SUCESS,
            answer: Answer.REGDATA_DB_ERROR
          })
        }
      })
      .catch(error => {
        dispatch({
          type: Actions.SEND_REGDATA_ERROR,
          answer: Answer.REGDATA_NO_RESPONSE
        })
      })
  }
}

export function changeUsername (value) {
  return {
    type: Actions.REG_CHANGE_USERNAME,
    username: value
  }
}

export function changePassword (value) {
  return {
    type: Actions.REG_CHANGE_PASSWORD,
    password: value
  }
}
