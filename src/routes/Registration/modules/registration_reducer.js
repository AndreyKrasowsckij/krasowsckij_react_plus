import { Actions } from './registration_actions'

const initialState = { reg_status: false, reg_fetching: false, password: '', username: '', answer: '' }
export default function registrationReducer (state = initialState, action) {
  switch (action.type) {
    case Actions.SEND_REGDATA_REQUEST:
      return { ...state, reg_fetching: true }

    case Actions.SEND_REGDATA_SUCESS:
      return { ...state, reg_fetching: false, reg_status: true, answer: action.answer }

    case Actions.SEND_REGDATA_ERROR:
      return { ...state, reg_status: false, reg_fetching: false, answer: action.answer }

    case Actions.REG_CHANGE_PASSWORD:
      return { ...state, password: action.password }

    case Actions.REG_CHANGE_USERNAME:
      return { ...state, username: action.username }

    default:
      return state
  }
}
