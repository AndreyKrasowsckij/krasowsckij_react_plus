import { injectReducer } from '../../store/reducers'

export default (store) => ({
  path: 'registration',
  getComponent (nextState, cb) {
    require.ensure([], (require) => {
      const Registration = require('./containers/RegistrationContainer').default
      const reducer = require('./modules/registration_reducer').default

      injectReducer(store, { key: 'registration', reducer })

      cb(null, Registration)
    }, 'registration')
  }
})
