import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { Answer } from './../modules/registration_answer'
import PageLayout from '../../../layouts/PageLayout/PageLayout'
import '../../../layouts/PageLayout/PageLayout.scss'

class Registration extends Component {
  constructor (props) {
    super(props)
    this.handleChangeUsername = this.handleChangeUsername.bind(this)
    this.handleChangePasword = this.handleChangePasword.bind(this)
  }

  handleChangeUsername (event) {
    const { changeUsername } = this.props
    changeUsername(event.target.value)
  }

  handleChangePasword (event) {
    const { changePassword } = this.props
    changePassword(event.target.value)
  }

  render () {
    const { regFetching } = this.props
    const { sendData } = this.props
    const { answer } = this.props
    const { authStatus } = this.props

    return (
      <div style={{ margin: '0 auto' }}>
        <PageLayout authStatus={authStatus} />
        <div className='page-layout__viewport'>
          <h5>{answer}</h5>
          <h2>Flag of fetching: {regFetching ? 'true' : 'false'}</h2>

          <input type='text' onChange={this.handleChangeUsername} />
          <input type='text' onChange={this.handleChangePasword} />
          <button className='btn btn-primary' onClick={sendData}>Registration</button>
        </div>
      </div>
    )
  }
}

Registration.propTypes = {
  authStatus: PropTypes.bool.isRequired,
  regFetching: PropTypes.bool.isRequired,
  sendData: PropTypes.func.isRequired,
  changeUsername: PropTypes.func.isRequired,
  changePassword: PropTypes.func.isRequired,
  answer: PropTypes.string.isRequired
}

export default Registration
