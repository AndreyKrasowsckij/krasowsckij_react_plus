import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { Answer } from './../modules/login_answer'
import PageLayout from '../../../layouts/PageLayout/PageLayout'
import '../../../layouts/PageLayout/PageLayout.scss'

class Login extends Component {
  constructor (props) {
    super(props)
    this.handleChangeUsername = this.handleChangeUsername.bind(this)
    this.handleChangePasword = this.handleChangePasword.bind(this)
  }

  handleChangeUsername (event) {
    const { changeUsername } = this.props
    changeUsername(event.target.value)
  }

  handleChangePasword (event) {
    const { changePassword } = this.props
    changePassword(event.target.value)
  }

  render () {
    const { loginStatus } = this.props
    const { loginFetching } = this.props
    const { sendData } = this.props
    const { answer } = this.props
    const { authStatus } = this.props

    return (
      <div style={{ margin: '0 auto' }}>
        <PageLayout authStatus={authStatus} />
        <div className='page-layout__viewport'>
          <h2>Flag of status: {loginStatus ? 'true' : 'false'}</h2>
          <h2>Flag of fetching: {loginFetching ? 'true' : 'false'}</h2>
          { answer }
          <input type='text' onChange={this.handleChangeUsername} />
          <input type='text' onChange={this.handleChangePasword} />
          <button className='btn btn-primary' onClick={sendData}>Login</button>
        </div>
      </div>
    )
  }
}

Login.propTypes = {
  changePassword: PropTypes.func.isRequired,
  changeUsername: PropTypes.func.isRequired,
  loginStatus: PropTypes.bool.isRequired,
  loginFetching: PropTypes.bool.isRequired,
  answer: PropTypes.bool.isRequired,
  authStatus: PropTypes.bool.isRequired,
  sendData: PropTypes.func.isRequired
}

export default Login
