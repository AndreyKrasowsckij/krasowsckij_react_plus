import { connect } from 'react-redux'
import { sendData, changePassword, changeUsername } from '../modules/login_creators'
import Login from '../components/Login'

const mapDispatchToProps = {
  sendData,
  changePassword,
  changeUsername
}

const mapStateToProps = (state) => ({
  loginStatus: state.login.login_status,
  loginFetching: state.login.login_fetching,
  authStatus: state.global_state.auth_status,
  answer: state.login.answer
})

export default connect(mapStateToProps, mapDispatchToProps)(Login)
