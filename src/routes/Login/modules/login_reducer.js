import { Actions } from './login_actions'

const initialState = { login_status: false, login_fetching: false, username: '', password: '', answer: '' }
export default function loginReducer (state = initialState, action) {
  switch (action.type) {
    case Actions.SEND_LOGDATA_REQUEST:
      return { ...state, login_fetching: true }

    case Actions.SEND_LOGDATA_SUCESS:
      return { ...state, login_status: true, login_fetching: false, answer: action.answer }

    case Actions.SEND_LOGDATA_ERROR:
      return { ...state, login_status: false, login_fetching: false }

    case Actions.LOG_CHANGE_PASSWORD:
      return { ...state, password: action.password }

    case Actions.LOG_CHANGE_USERNAME:
      return { ...state, username: action.username }

    default:
      return state
  }
}
