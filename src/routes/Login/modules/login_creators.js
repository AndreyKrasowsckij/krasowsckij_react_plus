import { serverAttr, localStoragePath } from './../../../confige'
import { Actions } from './login_actions'
import { Answer } from './login_answer'
import LocalStorage from 'localStorage'
import { browserHistory } from 'react-router'
import Axios from 'axios'
import { changeAuthorization } from './../../../store/global_state'

export function sendData () {
  return (dispatch, getState) => {
    dispatch({
      type: Actions.SEND_LOGDATA_REQUEST
    })
    Axios({
      method: 'post',
      url: serverAttr.login,
      data: {
        username: getState().login.username,
        password: getState().login.password
      }
    })
      .then(response => {
        if (response.data.type === Answer.LOGDATA_CORRECT) {
          dispatch({
            type: Actions.SEND_LOGDATA_SUCESS,
            answer: Answer.LOGDATA_CORRECT,
          })
          LocalStorage.setItem(localStoragePath.jwt, response.data.jwt)
          dispatch(changeAuthorization(true, response.data.id, response.data.jwt))
          browserHistory.push('/')
        }
        if (response.data.type === Answer.LOGDATA_PASSWORD_INCORRECT) {
          dispatch({
            type: Actions.SEND_LOGDATA_SUCESS,
            answer: Answer.LOGDATA_PASSWORD_INCORRECT
          })
        }
        if (response.data.type === Answer.LOGDATA_LOGIN_NOT_EXIST) {
          dispatch({
            type: Actions.SEND_LOGDATA_SUCESS,
            answer: Answer.LOGDATA_LOGIN_NOT_EXIST
          })
        }
        if (response.data.type === Answer.LOGDATA_DB_ERROR) {
          dispatch({
            type: Actions.SEND_LOGDATA_SUCESS,
            answer: Answer.LOGDATA_DB_ERROR
          })
        }
      })
      .catch(error => {
        dispatch({
          type: Actions.SEND_LOGDATA_ERROR
        })
      })
  }
}

export function changeUsername (value) {
  return {
    type: Actions.LOG_CHANGE_USERNAME,
    username: value
  }
}

export function changePassword (value) {
  return {
    type: Actions.LOG_CHANGE_PASSWORD,
    password: value
  }
}
