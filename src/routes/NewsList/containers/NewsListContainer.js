import { connect } from 'react-redux'
import { logOut } from './../../../store/global_state'
import {
  changeTags,
  changeThems,
  changeBody,
  saveNews,
  changeFilterTags,
  changeFilterThems,
  changeSearch,
  getNews,
  changePage } from '../modules/newslist_creators'
import NewsList from '../components/NewsList'

const mapDispatchToProps = {
  changeTags,
  changeThems,
  changeBody,
  saveNews,
  changeFilterTags,
  changeFilterThems,
  changeSearch,
  getNews,
  changePage
}

const mapStateToProps = (state) => ({
  id: state.global_state.id,
  saveStatus: state.news_list.save_status,
  saveFetching: state.news_list.save_fetching,
  saveAnswer: state.news_list.save_answer,
  getStatus: state.news_list.get_status,
  getFetching: state.news_list.get_fetching,
  answer: state.news_list.answer,
  news: state.news_list.news,
  currentPage: state.news_list.current_page,
  allCount: state.news_list.all_count,
})

export default connect(mapStateToProps, mapDispatchToProps)(NewsList)
