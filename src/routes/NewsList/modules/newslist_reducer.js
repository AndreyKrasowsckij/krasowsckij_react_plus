import { Actions } from './newslist_actions'

const initialState = {
  body: '',
  tags: '',
  thems: '',
  save_status: false,
  save_fetching: false,
  news: [],
  save_answer: '',
  filter_tags: '',
  filter_thems: '',
  search: '',
  get_status: false,
  get_fetching: false,
  answer: '',
  current_page: 0,
  all_count: 0
}
export default function newsListReducer (state = initialState, action) {
  switch (action.type) {

    case Actions.CHANGE_TAGS:
      return { ...state, tags: action.tags }

    case Actions.CHANGE_THEMS:
      return { ...state, thems: action.thems }

    case Actions.CHANGE_BODY:
      return { ...state, body: action.body }

    case Actions.SAVE_NEWS_REQUEST:
      return { ...state, save_fetching: true }

    case Actions.SAVE_NEWS_SUCESS:
      return { ...state, save_status: true, save_fetching: false, save_answer: action.save_answer }

    case Actions.SAVE_NEWS_ERROR:
      return { ...state, save_status: false, save_fetching: false }

    case Actions.CHANGE_FILTER_TAGS:
      return { ...state, filter_tags: action.filter_tags }

    case Actions.CHANGE_FILTER_THEMS:
      return { ...state, filter_thems: action.filter_thems }

    case Actions.CHANGE_SEARCH:
      return { ...state, search: action.search }

    case Actions.GET_NEWS_REQUEST:
      return { ...state, get_fetching: true }

    case Actions.GET_NEWS_SUCESS:
      return { ...state,
        get_status: true,
        get_fetching: false,
        answer: action.answer,
        news: action.news,
        all_count: action.all_count
      }

    case Actions.GET_NEWS_ERROR:
      return { ...state, get_status: false, get_fetching: false }

    case Actions.CHANGE_PAGE:
      return { ...state, current_page: action.current_page }

    default:
      return state
  }
}
