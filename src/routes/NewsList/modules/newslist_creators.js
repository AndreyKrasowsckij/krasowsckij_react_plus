import { serverAttr, localStoragePath } from './../../../confige'
import { Actions } from './newslist_actions'
import { Answer } from './newslist_answer'
import LocalStorage from 'localStorage'
import { browserHistory } from 'react-router'
import Axios from 'axios'
import { changeAuthorization } from './../../../store/global_state'
import FormData from 'form-data'
import { pageConf } from './../../../confige'

export function changeTags (value) {
  return {
    type: Actions.CHANGE_TAGS,
    tags: value
  }
}

export function changeThems (value) {
  return {
    type: Actions.CHANGE_THEMS,
    thems: value
  }
}

export function changeBody (value) {
  return {
    type: Actions.CHANGE_BODY,
    body: value
  }
}

export function saveNews (id) {
  return (dispatch, getState) => {
    let image = document.getElementById('image-news').files[0]
    let formState = new FormData()
    formState.append('image', image)
    formState.append('id', getState().global_state.id)
    formState.append('tags', getState().news_list.tags)
    formState.append('thems', getState().news_list.thems)
    formState.append('body', getState().news_list.body)

    dispatch({
      type: Actions.SAVE_NEWS_REQUEST
    })
    Axios({
      method: 'post',
      url: serverAttr.newslist,
      data: formState
    })
      .then(response => {
        if (response.data.type === Answer.CORRECT_SAVE_NEWS) {
          dispatch({
            type: Actions.SAVE_NEWS_SUCESS,
            save_answer: Answer.CORRECT_SAVE_NEWS
          })
        }
        if (response.data.type === Answer.USRDATA_LOGIN_NOT_EXIST) {
          dispatch({
            type: Actions.SAVE_NEWS_SUCESS,
            save_answer: Answer.USRDATA_LOGIN_NOT_EXIST
          })
        }
        if (response.data.type === Answer.USRDATA_DB_ERROR) {
          dispatch({
            type: Actions.SAVE_NEWS_SUCESS,
            save_answer: Answer.USRDATA_DB_ERROR
          })
        }
        if (response.data.type === Answer.NEWS_DB_ERROR) {
          dispatch({
            type: Actions.SAVE_NEWS_SUCESS,
            save_answer: Answer.NEWS_DB_ERROR
          })
        }
      })
      .catch(error => {
        dispatch({
          type: Actions.SAVE_NEWS_ERROR
        })
      })
    getNewsFunc(id, dispatch, getState)
  }
}

export function changeFilterTags (value) {
  return {
    type: Actions.CHANGE_FILTER_TAGS,
    filter_tags: value
  }
}

export function changeFilterThems (value) {
  return {
    type: Actions.CHANGE_FILTER_THEMS,
    filter_thems: value
  }
}

export function changeSearch (value) {
  return {
    type: Actions.CHANGE_SEARCH,
    search: value
  }
}

export function getNews (id) {
  return (dispatch, getState) => {
    getNewsFunc(id, dispatch, getState)
  }
}

export function changePage (value, id) {
  return (dispatch, getState) => {
    dispatch({
      type: Actions.CHANGE_PAGE,
      current_page: value
    })
    getNewsFunc(id, dispatch, getState)
  }
}

let getNewsFunc = (id, dispatch, getState) => {
  dispatch({
    type: Actions.GET_NEWS_REQUEST
  })
  Axios({
    method: 'post',
    url: serverAttr.news,
    data: {
      id: id,
      filter_tags: getState().news_list.filter_tags,
      filter_thems: getState().news_list.filter_thems,
      search: getState().news_list.search,
      page_count: pageConf.count,
      current_page: getState().news_list.current_page
    }
  })
    .then(response => {
      if (response.data.type === Answer.GET_NEWS_CORRECT) {
        dispatch({
          type: Actions.GET_NEWS_SUCESS,
          news: response.data.news,
          all_count: response.data.all_count,
          answer: Answer.GET_NEWS_CORRECT
        })
      }
      if (response.data.type === Answer.NEWS_DB_ERROR) {
        dispatch({
          type: Actions.GET_NEWS_SUCESS,
          answer: Answer.NEWS_DB_ERROR
        })
      }
    })
    .catch(error => {
      dispatch({
        type: Actions.GET_NEWS_ERROR
      })
    })
}
