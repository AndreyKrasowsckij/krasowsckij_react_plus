import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { Answer } from './../modules/newslist_answer'
import '../../../layouts/PageLayout/PageLayout.scss'
import './NewsList.scss'
import { pageConf } from './../../../confige'
import { IndexLink, Link } from 'react-router'

class NewsList extends Component {
  constructor (props) {
    super(props)
    this.handleChangeTags = this.handleChangeTags.bind(this)
    this.handleChangeThems = this.handleChangeThems.bind(this)
    this.handleChangeBody = this.handleChangeBody.bind(this)
    this.handleChangeFiterThems = this.handleChangeFiterThems.bind(this)
    this.handleChangeFilterTags = this.handleChangeFilterTags.bind(this)
    this.handleChangeSearch = this.handleChangeSearch.bind(this)
    this.clickGetNews = this.clickGetNews.bind(this)
    this.clickChangePage = this.clickChangePage.bind(this)
    this.clickSaveNews = this.clickSaveNews.bind(this)
  }

  clickSaveNews () {
    const { saveNews } = this.props
    const { parent } = this.props
    switch (parent) {
      case 'Users':
        const { parent_id } = this.props
        saveNews(parent_id)
        break

      case 'News':
        saveNews(-1)
        break
    }
  }

  clickChangePage (event) {
    const { changePage } = this.props
    const { parent } = this.props
    switch (parent) {
      case 'Users':
        const { parent_id } = this.props
        changePage(event.target.value, parent_id)
        break

      case 'News':
        changePage(event.target.value, -1)
        break
    }
  }

  handleChangeFiterThems (event) {
    const { changeFilterThems } = this.props
    changeFilterThems(event.target.value)
  }

  handleChangeFilterTags (event) {
    const { changeFilterTags } = this.props
    changeFilterTags(event.target.value)
  }

  handleChangeSearch (event) {
    const { changeSearch } = this.props
    changeSearch(event.target.value)
  }

  handleChangeBody (event) {
    const { changeBody } = this.props
    changeBody(event.target.value)
  }

  handleChangeTags (event) {
    const { changeTags } = this.props
    changeTags(event.target.value)
  }

  handleChangeThems (event) {
    const { changeThems } = this.props
    changeThems(event.target.value)
  }

  componentWillMount () {
    const { changePage } = this.props
    const { getNews } = this.props
    const { parent } = this.props
    const { changeFilterThems } = this.props
    const { changeFilterTags } = this.props
    const { changeSearch } = this.props
    changeSearch('')
    changeFilterTags('')
    changeFilterThems('')
    switch (parent) {
      case 'Users':
        const { parent_id } = this.props
        changePage(0, parent_id)
        break

      case 'News':
        changePage(0, -1)
        break
    }
  }

  clickGetNews () {
    const { changePage } = this.props
    const { getNews } = this.props
    const { parent } = this.props
    switch (parent) {
      case 'Users':
        const { parent_id } = this.props
        changePage(0, parent_id)
        break

      case 'News':
        changePage(0, -1)
        break
    }
  }

  render () {
    const { parent } = this.props
    const { saveStatus } = this.props
    const { saveFetching } = this.props
    const { saveAnswer } = this.props
    const { saveNews } = this.props
    const { getStatus } = this.props
    const { getFetching } = this.props
    const { answer } = this.props
    const { news } = this.props
    const { allCount } = this.props
    const { id } = this.props
    const { parent_id } = this.props

    let classList = 'news-list'
    let newsHide = 'add-news news-list__add-news'
    switch (parent) {
      case 'Users':
        classList += ' news-list_users'
        break

      case 'Guest':
        classList += ' news-list_news'
        newsHide += ' hiden'
        break

      case 'News':
        classList += ' news-list_news'
        newsHide += ' hiden'
        break

      default:
        break
    }

    const buttonArray = []
    for (let i = 0; i < allCount; i++) {
      buttonArray.push(<button value={i} className='button-counter' onClick={this.clickChangePage}>{i}</button>)
    }

    return (
      <div className={classList}>
        <div className={newsHide}>
          <button className='btn btn-primary' onClick={this.clickSaveNews}>Add news</button>
          <div>
            <h5>saveStatus: {saveStatus ? 'true' : 'false'}</h5>
            <h5>saveFetching: {saveFetching ? 'true' : 'false'}</h5>
            <h5>saveAnswer: {saveAnswer}</h5>
            <input type='file' id='image-news' name='image' />
          </div>
          <textarea onChange={this.handleChangeBody} />
          <div className='add-news__input'>
            <input placeholder='Tags' onChange={this.handleChangeTags} />
            <input placeholder='Thems' onChange={this.handleChangeThems} />
          </div>
        </div>
        <div className='filter-news news-list__filter-news'>
          <button className='btn btn-primary' onClick={this.clickGetNews}>Filter</button>
          <input placeholder='Tags' onChange={this.handleChangeFilterTags} />
          <input placeholder='Thems' onChange={this.handleChangeFiterThems} />
          <input placeholder='What find?' onChange={this.handleChangeSearch} />
        </div>
        <h5>getStatus: {getStatus ? 'true' : 'false'}</h5>
        <h5>getFetching: {getFetching ? 'true' : 'false'}</h5>
        <h5>answer: {answer}</h5>
        {news.map((value) =>
          <div className='small-news'>
            <img className='news__img' src={value.image} />
            <h5 > Body <br /> {value.body}</h5>
            <h5>Tags<br />{value.tags.map(tag => tag + ' ')}</h5>
            <h5>Thems<br />{value.thems.map(them => them + ' ')}</h5>
            <Link to={path_lord(value.lord)}>Lord</Link>
          </div>
        )}
        <div className='counter'>{buttonArray}</div>
      </div>
    )
  }
}
NewsList.propTypes = {
  parent: PropTypes.string.isRequired,
  changeThems: PropTypes.func.isRequired,
  changeTags: PropTypes.func.isRequired,
  changeBody: PropTypes.func.isRequired,
  saveNews: PropTypes.func.isRequired,
  saveFetching: PropTypes.bool.isRequired,
  saveStatus: PropTypes.bool.isRequired,
  saveAnswer: PropTypes.string.isRequired,
  changeFilterTags: PropTypes.func.isRequired,
  changeFilterThems: PropTypes.func.isRequired,
  changeSearch: PropTypes.func.isRequired,
  getNews: PropTypes.func.isRequired,
  getFetching: PropTypes.bool.isRequired,
  getStatus: PropTypes.bool.isRequired,
  answer: PropTypes.string.isRequired,
  news: PropTypes.object.isRequired,
  changePage: PropTypes.func.isRequired,
  currentPage: PropTypes.number.isRequired,
  allCount: PropTypes.number.isRequired,
  parent_id: PropTypes.number.isRequired,
  id: PropTypes.number.isRequired
}
export default NewsList

let pageCounter = (mas) => {
  let allPage = (mas.length - (mas.length % pageConf.count)) / pageConf.count
  if (mas.length % pageConf.count !== 0) { allPage += 1 }
  return allPage
}

let path_lord = (val) => {
  return `/guest/${val}`
}
