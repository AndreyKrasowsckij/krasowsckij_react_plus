import { connect } from 'react-redux'
import { logOut } from './../../../store/global_state'
import Guest from '../components/Guest'
import { getGuestData } from './../modules/guest_creators'

const mapDispatchToProps = {
  logOut,
  getGuestData
}

const mapStateToProps = (state) => ({
  authStatus: state.global_state.auth_status,
  id: state.global_state.id,
  guestStatus: state.guest.guest_status,
  guestFetching: state.guest.guest_fetching,
  answer: state.guest.answer,
  userName: state.guest.user_name,
  firstName: state.guest.first_name,
  secondName: state.guest.second_name,
  age: state.guest.age,
  image: state.guest.image
})

export default connect(mapStateToProps, mapDispatchToProps)(Guest)
