import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { Answer } from './../modules/guest_answer'
import PageLayout from '../../../layouts/PageLayout/PageLayout'
import '../../../layouts/PageLayout/PageLayout.scss'
import NewsList from './../../NewsList/containers/NewsListContainer'
import './Guest.scss'

class Guest extends Component {
  constructor (props) {
    super(props)
  }

  componentWillMount () {
    const { getGuestData } = this.props
    const { userId } = this.props.params
    getGuestData(userId)
  }

  render () {
    const { authStatus } = this.props
    const { logOut } = this.props
    const { userId } = this.props.params
    const { guestStatus } = this.props
    const { guestFetching } = this.props
    const { answer } = this.props
    const { userName } = this.props
    const { firstName } = this.props
    const { secondName } = this.props
    const { age } = this.props
    const { image } = this.props

    return (
      <div style={{ margin: '0 auto' }}>
        <PageLayout authStatus={authStatus} logOut={logOut} />
        <div className='page-layout__viewport'>
          <h5>Guest</h5>
          <h5 className='text' >guestStatus: {guestStatus ? 'true' : 'false'}</h5>
          <h5 className='text' >guestFetching: {guestFetching ? 'true' : 'false'}</h5>
          <h5 className='text' >answer: {answer}</h5>
        </div>
        <div className='info'>
          <div className='photo'>
            <img className='photo__img_new' src={'http://localhost:3000/' + image} />
          </div>
          <div>
            <h5 className='text' >userName: {userName}</h5>
            <h5 className='text' >firstName: {firstName}</h5>
            <h5 className='text' >secondName: {secondName}</h5>
            <h5 className='text' >age: {age}</h5>
          </div>
        </div>
        <NewsList parent='Guest' parent_id={userId} />
      </div>
    )
  }
}
Guest.propTypes = {
  authStatus: PropTypes.bool.isRequired,
  logOut: PropTypes.func.isRequired,
  userId: PropTypes.string.isRequired,
  guestStatus: PropTypes.bool.isRequired,
  getGuestData: PropTypes.func.isRequired,
  guestFetching: PropTypes.bool.isRequired,
  answer: PropTypes.string.isRequired,
  userName: PropTypes.string.isRequired,
  firstName: PropTypes.string.isRequired,
  secondName: PropTypes.string.isRequired,
  age: PropTypes.number.isRequired,
  image: PropTypes.string.isRequired,
}
export default Guest

/*
class Guest extends Component {
  constructor (props) {
    super(props)
  }

  componentWillMount () {
    const { getGuestData } = this.props
    const { userId } = this.props.params
    getGuestData(userId)
  }

  render () {
    const { authStatus } = this.props
    const { logOut } = this.props
    const { userId } = this.props.params
    const { guestStatus } = this.props
    const { guestFetching } = this.props
    const { answer } = this.props

    return (
      <div style={{ margin: '0 auto' }}>
        <PageLayout authStatus={authStatus} logOut={logOut} />
        <div className='page-layout__viewport'>
          <h5>Guest</h5>
          <h2>guestStatus: {guestStatus ? 'true' : 'false'}</h2>
          <h2>guestFetching: {guestFetching ? 'true' : 'false'}</h2>
          <h2>Answer: {answer}</h2>
          <h5>User id: {userId}</h5>
        </div>
      </div>
    )
  }
}
Guest.propTypes = {
  authStatus: PropTypes.bool.isRequired,
  logOut: PropTypes.func.isRequired,
  userId: PropTypes.string.isRequired,
  getGuestData: PropTypes.func.isRequired,
  guestStatus: PropTypes.bool.isRequired,
  guestFetching: PropTypes.bool.isRequired,
  answer: PropTypes.string.isRequired
}

export default Guest
 */
