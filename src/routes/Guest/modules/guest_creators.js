import { serverAttr, localStoragePath } from './../../../confige'
import { Actions } from './guest_actions'
import { Answer } from './guest_answer'
import LocalStorage from 'localStorage'
import Axios from 'axios'
import { changeAuthorization } from './../../../store/global_state'
import FormData from 'form-data'
import { browserHistory } from 'react-router'

export function getGuestData (id) {
  return (dispatch, getState) => {
    if (typeof (parseInt(id)) === 'number') {
      if (parseInt(id) === getState().global_state.id) {
        browserHistory.push('/users')
      }

      dispatch({
        type: Actions.SEND_GUESTDATA_REQUEST
      })
      Axios({
        method: 'post',
        url: serverAttr.users,
        data: {
          id: parseInt(id)
        }
      })
        .then(response => {
          if (response.data.type === Answer.USRDATA_CORRECT) {
            dispatch({
              type: Actions.SEND_GUESTDATA_SUCESS,
              answer: response.data.type,
              user_name: response.data.user_name,
              first_name: response.data.first_name,
              second_name: response.data.second_name,
              age: response.data.age,
              image: response.data.image
            })
          }
          if (response.data.type === Answer.USRDATA_LOGIN_NOT_EXIST) {
            dispatch({
              type: Actions.SEND_GUESTDATA_SUCESS,
              answer: response.data.type
            })
          }
          if (response.data.type === Answer.USRDATA_DB_ERROR) {
            dispatch({
              type: Actions.SEND_GUESTDATA_SUCESS,
              answer: response.data.type
            })
          }
        })
        .catch(error => {
          dispatch({
            type: Actions.SEND_GUESTDATA_ERROR
          })
        })
    }
  }
}
