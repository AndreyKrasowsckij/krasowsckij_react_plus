import { Actions } from './guest_actions'

const initialState = {
  guest_status: false,
  guest_fetching: false,
  user_name: '',
  first_name: '',
  second_name: '',
  age: 0,
  image: '',
  answer: ''
}
export default function guestReducer (state = initialState, action) {
  switch (action.type) {
    case Actions.SEND_GUESTDATA_REQUEST:
      return { ...state, guest_fetching: true }

    case Actions.SEND_GUESTDATA_SUCESS:
      return { ...state,
        guest_status: true,
        guest_fetching: false,
        user_name: action.user_name,
        first_name: action.first_name,
        second_name: action.second_name,
        age: action.age,
        image: action.image,
        answer: action.answer
      }

    case Actions.SEND_GUESTDATA_ERROR:
      return { ...state, guest_status: false, guest_fetching: false }

    default:
      return state
  }
}
