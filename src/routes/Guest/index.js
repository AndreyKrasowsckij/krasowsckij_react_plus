import { injectReducer } from '../../store/reducers'

export default (store) => ({
  path: 'guest/:userId',
  getComponent (nextState, cb) {
    require.ensure([], (require) => {
      const Guest = require('./containers/GuestCotainer').default
      const reducer = require('./modules/guest_reducer').default

      injectReducer(store, { key: 'guest', reducer })

      cb(null, Guest)
    }, 'guest')
  }
})
