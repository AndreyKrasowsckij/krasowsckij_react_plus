import { injectReducer } from '../../store/reducers'

export default (store) => ({
  path: 'users',
  getComponent (nextState, cb) {
    require.ensure([], (require) => {
      const Registration = require('./containers/UsersContainer').default
      const reducer = require('./modules/users_reducer').default

      injectReducer(store, { key: 'users', reducer })

      cb(null, Registration)
    }, 'users')
  }
})
