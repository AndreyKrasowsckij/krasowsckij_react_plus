import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { Answer } from './../modules/users_answer'
import PageLayout from '../../../layouts/PageLayout/PageLayout'
import NewsList from './../../NewsList/containers/NewsListContainer'
import '../../../layouts/PageLayout/PageLayout.scss'
import './Users.scss'

class Users extends Component {
  constructor (props) {
    super(props)
    this.getInputFields = this.getInputFields.bind(this)
    this.handleChangeFirstName = this.handleChangeFirstName.bind(this)
    this.handleChangeSecondName = this.handleChangeSecondName.bind(this)
    this.handleChangeAge = this.handleChangeAge.bind(this)
  }

  handleChangeFirstName (event) {
    const { changeFirstName } = this.props
    changeFirstName(event.target.value)
  }

  handleChangeSecondName (event) {
    const { changeSecondName } = this.props
    changeSecondName(event.target.value)
  }

  handleChangeAge (event) {
    const { changeAge } = this.props
    changeAge(event.target.value)
  }

  getInputFields () {
    const { changeInfo } = this.props
    changeInfo(true)
  }

  componentWillMount () {
    const { getUserData } = this.props
    getUserData()
  }

  render () {
    const { authStatus } = this.props
    const { logOut } = this.props
    const { usrStatus } = this.props
    const { usrFetching } = this.props
    const { userName } = this.props
    const { firstName } = this.props
    const { secondName } = this.props
    const { age } = this.props
    const { image } = this.props
    const { answer } = this.props
    const { changePhoto } = this.props
    const { imgStatus } = this.props
    const { imgFetching } = this.props
    const { imgAnswer } = this.props
    const { infoChange } = this.props
    const { saveInfo } = this.props
    const { infoStatus } = this.props
    const { infoFetching } = this.props
    const { infoAnswer } = this.props
    const { id } = this.props

    let infoBlock
    if (!infoChange) {
      infoBlock = <div className='data'>
          <button className='btn btn-primary' onClick={this.getInputFields}>Change info</button>
          <h5>First name: {firstName}</h5>
          <h5>Second name: {secondName}</h5>
          <h5>Age: {age}</h5>
        </div>
    } else {
      infoBlock = <div className='data'>
          <button className='btn btn-primary' onClick={saveInfo}>Save</button>
          <div className='data__info'>
            <h5>First name: </h5>
            <input className='data__input' placeholder='First name' value={firstName} onChange={this.handleChangeFirstName} />
          </div>
          <div className='data__info'>
            <h5>Second name: </h5>
            <input className='data__input' placeholder='Second name' value={secondName} onChange={this.handleChangeSecondName} />
          </div>
          <div className='data__info'>
            <h5>Age: </h5>
            <input className='data__input' placeholder='Age' value={age} onChange={this.handleChangeAge} />
          </div>
        </div>
    }

    return (
      <div style={{ margin: '0 auto' }}>
        <PageLayout authStatus={authStatus} logOut={logOut} />
        <h5 className='text' >usrStatus: {usrStatus ? 'true' : 'false'}</h5>
        <h5 className='text' >usrFetching: {usrFetching ? 'true' : 'false'}</h5>
        <h5 className='text' >userName: {userName}</h5>
        <h5 className='text' >answer: {answer}</h5>
        <div className='users'>
          <div className='photo'>
            <h5 className='text' >Photo status: {imgStatus ? 'true' : 'false'}</h5>
            <h5 className='text' >Photo fetching: {imgFetching ? 'true' : 'false'}</h5>
            <h5 className='text' >imgAnswer: {imgAnswer}</h5>
            <img className='photo__img' src={image} />
            <input type='file' id='photo_user' name='image' onChange={changePhoto} />
          </div>
          <div>
            <h5 className='text' >Info status: {infoStatus ? 'true' : 'false'}</h5>
            <h5 className='text' >Info fetching: {infoFetching ? 'true' : 'false'}</h5>
            <h5 className='text' >infoAnswer: {infoAnswer}</h5>
            {infoBlock}
          </div>
        </div>
        <NewsList parent='Users' parent_id={id} />
      </div>
    )
  }
}
Users.propTypes = {
  authStatus: PropTypes.bool.isRequired,
  imgStatus: PropTypes.bool.isRequired,
  imgFetching: PropTypes.bool.isRequired,
  logOut: PropTypes.func.isRequired,
  changeInfo: PropTypes.func.isRequired,
  changePhoto: PropTypes.func.isRequired,
  getUserData: PropTypes.func.isRequired,
  usrStatus: PropTypes.bool.isRequired,
  usrFetching: PropTypes.bool.isRequired,
  userName: PropTypes.string.isRequired,
  firstName: PropTypes.string.isRequired,
  secondName: PropTypes.string.isRequired,
  infoAnswer: PropTypes.string.isRequired,
  age: PropTypes.number.isRequired,
  image: PropTypes.string.isRequired,
  answer: PropTypes.string.isRequired,
  imgAnswer: PropTypes.string.isRequired,
  infoChange: PropTypes.bool.isRequired,
  changeFirstName: PropTypes.func.isRequired,
  changeSecondName: PropTypes.func.isRequired,
  changeAge: PropTypes.func.isRequired,
  saveInfo: PropTypes.func.isRequired,
  infoStatus: PropTypes.bool.isRequired,
  infoFetching: PropTypes.bool.isRequired,
  id: PropTypes.number.isRequired
}

export default Users
