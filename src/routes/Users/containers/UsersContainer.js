import { connect } from 'react-redux'
import { logOut } from './../../../store/global_state'
import {
  getUserData,
  changePhoto,
  changeInfo,
  changeFirstName,
  changeSecondName,
  changeAge,
  saveInfo } from './../modules/users_creators'
import Users from '../components/Users'

const mapDispatchToProps = {
  logOut,
  getUserData,
  changePhoto,
  changeInfo,
  changeFirstName,
  changeSecondName,
  changeAge,
  saveInfo
}

const mapStateToProps = (state) => ({
  authStatus: state.global_state.auth_status,
  id: state.global_state.id,
  usrStatus: state.users.usr_status,
  usrFetching: state.users.usr_fetching,
  userName: state.users.user_name,
  firstName: state.users.first_name,
  secondName: state.users.second_name,
  age: state.users.age,
  image: state.users.image,
  answer: state.users.answer,
  imgStatus: state.users.img_status,
  imgFetching: state.users.img_fetching,
  imgAnswer: state.users.img_answer,
  infoChange: state.users.info_change,
  infoStatus: state.users.info_status,
  infoFetching: state.users.info_fetching,
  infoAnswer: state.users.info_answer
})

export default connect(mapStateToProps, mapDispatchToProps)(Users)
