export const Actions = {
  SEND_USRDATA_REQUEST: 'SEND_USRDATA_REQUEST',
  SEND_USRDATA_SUCESS: 'SEND_USRDATA_SUCESS',
  SEND_USRDATA_ERROR: 'SEND_USRDATA_ERROR',
  USR_CHANGE_USRNAME: 'USR_CHANGE_USRNAME',
  USR_CHANGE_FRTNAME: 'USR_CHANGE_FRTNAME',
  USR_CHANGE_SCDNAME: 'USR_CHANGE_SCDNAME',
  USR_CHANGE_AGE: 'USR_CHANGE_AGE',
  USR_CHANGE_IMAGE_REQUEST: 'USR_CHANGE_IMAGE_REQUEST',
  USR_CHANGE_IMAGE_SUCESS: 'USR_CHANGE_IMAGE_SUCESS',
  USR_CHANGE_IMAGE_ERROR: 'USR_CHANGE_IMAGE_ERROR',
  INFO_CHANGE: 'INFO_CHANGE',
  INFO_CHANGE_FIRSTNAME: 'INFO_CHANGE_FIRSTNAME',
  INFO_CHANGE_SECONDNAME: 'INFO_CHANGE_SECONDNAME',
  INFO_CHANGE_AGE: 'INFO_CHANGE_AGE',
  SEND_INFO_REQUEST: 'SEND_INFO_REQUEST',
  SEND_INFO_ERROR: 'SEND_INFO_ERROR',
  SEND_INFO_SUCESS: 'SEND_INFO_SUCESS'
}
