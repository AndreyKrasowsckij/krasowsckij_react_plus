import { Actions } from './users_actions'

const initialState = {
  usr_status: false,
  usr_fetching: false,
  user_name: '',
  first_name: '',
  second_name: '',
  age: 0,
  image: '',
  answer: '',
  img_status: false,
  img_fetching: false,
  img_answer: '',
  info_change: false,
  info_status: false,
  info_fetching: false,
  info_answer: ''
}

export default function usersReducer (state = initialState, action) {
  switch (action.type) {
    case Actions.SEND_USRDATA_REQUEST:
      return { ...state, usr_fetching: true }

    case Actions.SEND_USRDATA_SUCESS:
      return { ...state,
        usr_status: true,
        usr_fetching: false,
        user_name: action.user_name,
        first_name: action.first_name,
        second_name: action.second_name,
        age: action.age,
        image: action.image,
        answer: action.answer
      }

    case Actions.SEND_USRDATA_ERROR:
      return { ...state, usr_status: false, usr_fetching: false, answer: action.answer }

    case Actions.USR_CHANGE_USRNAME:
      return { ...state, user_name: action.user_name }

    case Actions.USR_CHANGE_FRTNAME:
      return { ...state, first_name: action.first_name }

    case Actions.USR_CHANGE_SCDNAME:
      return { ...state, second_name: action.second_name }

    case Actions.USR_CHANGE_AGE:
      return { ...state, age: action.age }

    case Actions.USR_CHANGE_IMAGE_REQUEST:
      return { ...state, img_fetching: true }

    case Actions.USR_CHANGE_IMAGE_SUCESS:
      return { ...state, img_status: true, img_fetching: false, img_answer: action.img_answer, image: action.image }

    case Actions.USR_CHANGE_IMAGE_ERROR:
      return { ...state, img_status: false, img_fetching: false, img_answer: action.img_answer }

    case Actions.INFO_CHANGE:
      return { ...state, info_change: action.info_change }

    case Actions.INFO_CHANGE_FIRSTNAME:
      return { ...state, first_name: action.first_name }

    case Actions.INFO_CHANGE_SECONDNAME:
      return { ...state, second_name: action.second_name }

    case Actions.INFO_CHANGE_AGE:
      return { ...state, age: action.age }

    case Actions.SEND_INFO_REQUEST:
      return { ...state, info_fetching: true }

    case Actions.SEND_INFO_SUCESS:
      return { ...state,
        info_fetching: false,
        info_status: true,
        info_answer: action.info_answer,
        first_name: action.first_name,
        second_name: action.second_name,
        age: action.age
      }

    case Actions.SEND_INFO_ERROR:
      return { ...state, info_fetching: false, info_status: false }

    default:
      return state
  }
}
