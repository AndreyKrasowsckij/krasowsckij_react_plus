import { serverAttr, localStoragePath } from './../../../confige'
import { Actions } from './users_actions'
import { Answer } from './users_answer'
import LocalStorage from 'localStorage'
import { browserHistory } from 'react-router'
import Axios from 'axios'
import { changeAuthorization } from './../../../store/global_state'
import FormData from 'form-data'

export function getUserData () {
  return (dispatch, getState) => {
    dispatch({
      type: Actions.SEND_USRDATA_REQUEST
    })
    Axios({
      method: 'post',
      url: serverAttr.users,
      data: {
        id: getState().global_state.id
      }
    })
      .then(response => {
        if (response.data.type === Answer.USRDATA_CORRECT) {
          dispatch({
            type: Actions.SEND_USRDATA_SUCESS,
            answer: response.data.type,
            user_name: response.data.user_name,
            first_name: response.data.first_name,
            second_name: response.data.second_name,
            age: response.data.age,
            image: response.data.image
          })
        }
        if (response.data.type === Answer.USRDATA_LOGIN_NOT_EXIST) {
          dispatch({
            type: Actions.SEND_USRDATA_SUCESS,
            answer: response.data.type
          })
        }
        if (response.data.type === Answer.USRDATA_DB_ERROR) {
          dispatch({
            type: Actions.SEND_USRDATA_SUCESS,
            answer: response.data.type
          })
        }
      })
      .catch(error => {
        dispatch({
          type: Actions.SEND_USRDATA_ERROR
        })
      })
  }
}

export function changePhoto (value) {
  return (dispatch, getState) => {
    let image = value.target.files[0]
    let formState = new FormData()
    formState.append('image', image)
    formState.append('id', getState().global_state.id)

    dispatch({
      type: Actions.USR_CHANGE_IMAGE_REQUEST
    })
    Axios({
      method: 'post',
      url: serverAttr.image,
      data: formState
    })
      .then(response => {
        if (response.data.type === Answer.USRDATA_CORRECT) {
          dispatch({
            type: Actions.USR_CHANGE_IMAGE_SUCESS,
            image: response.data.image,
            img_answer: response.data.type
          })
        }
        if (response.data.type === Answer.USRDATA_LOGIN_NOT_EXIST) {
          dispatch({
            type: Actions.USR_CHANGE_IMAGE_SUCESS,
            img_answer: response.data.type
          })
        }
        if (response.data.type === Answer.USRDATA_DB_ERROR) {
          dispatch({
            type: Actions.USR_CHANGE_IMAGE_SUCESS,
            img_answer: response.data.type
          })
        }
      })
      .catch(error => {
        dispatch({
          type: Actions.USR_CHANGE_IMAGE_ERROR
        })
      })
  }
}

export function changeInfo (value) {
  return {
    type: Actions.INFO_CHANGE,
    info_change: value
  }
}

export function changeFirstName (value) {
  return {
    type: Actions.INFO_CHANGE_FIRSTNAME,
    first_name: value
  }
}

export function changeSecondName (value) {
  return {
    type: Actions.INFO_CHANGE_SECONDNAME,
    second_name: value
  }
}

export function changeAge (value) {
  return {
    type: Actions.INFO_CHANGE_AGE,
    age: value
  }
}

export function saveInfo () {
  return (dispatch, getState) => {
    dispatch({
      type: Actions.SEND_INFO_REQUEST
    })
    Axios({
      method: 'post',
      url: serverAttr.info,
      data: {
        id: getState().global_state.id,
        first_name: getState().users.first_name,
        second_name: getState().users.second_name,
        age: getState().users.age
      }
    })
      .then(response => {
        if (response.data.type === Answer.USRDATA_CORRECT) {
          dispatch({
            type: Actions.SEND_INFO_SUCESS,
            info_answer: response.data.type,
            first_name: response.data.first_name,
            second_name: response.data.second_name,
            age: response.data.age
          })
        }
        if (response.data.type === Answer.USRDATA_LOGIN_NOT_EXIST) {
          dispatch({
            type: Actions.SEND_INFO_SUCESS,
            info_answer: response.data.type,
            first_name: response.data.first_name,
            second_name: response.data.second_name,
            age: response.data.age
          })
        }
        if (response.data.type === Answer.USRDATA_DB_ERROR) {
          dispatch({
            type: Actions.SEND_INFO_SUCESS,
            info_answer: response.data.type,
            first_name: response.data.first_name,
            second_name: response.data.second_name,
            age: response.data.age
          })
        }
        dispatch(changeInfo(false))
      })
      .catch(error => {
        dispatch({
          type: Actions.SEND_INFO_ERROR
        })
      })
  }
}
