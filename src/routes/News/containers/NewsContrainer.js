import { connect } from 'react-redux'
import { logOut } from './../../../store/global_state'
import News from '../components/News'

const mapDispatchToProps = {
  logOut
}

const mapStateToProps = (state) => ({
  authStatus: state.global_state.auth_status
})

export default connect(mapStateToProps, mapDispatchToProps)(News)
