import { injectReducer } from '../../store/reducers'

export default (store) => ({
  getComponent (nextState, cb) {
    require.ensure([], (require) => {
      const News = require('./containers/NewsContrainer').default
      const reducer = require('./modules/news_reducer').default

      injectReducer(store, { key: 'news', reducer })

      cb(null, News)
    }, 'registration')
  }
})
