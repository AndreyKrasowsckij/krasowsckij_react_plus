import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { Answer } from './../modules/news_answer'
import PageLayout from '../../../layouts/PageLayout/PageLayout'
import '../../../layouts/PageLayout/PageLayout.scss'
import NewsList from './../../NewsList/containers/NewsListContainer'

class News extends Component {
  constructor (props) {
    super(props)
  }

  render () {
    const { authStatus } = this.props
    const { logOut } = this.props

    return (
      <div style={{ margin: '0 auto' }}>
        <PageLayout authStatus={authStatus} logOut={logOut} />
        <div className='page-layout__viewport'>
          <h5>News</h5>
        </div>
        <NewsList parent='News' />
      </div>
    )
  }
}
News.propTypes = {
  authStatus: PropTypes.bool.isRequired,
  logOut: PropTypes.func.isRequired
}

export default News
