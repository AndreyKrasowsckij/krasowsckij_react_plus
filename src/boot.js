import { changeAuthorization } from 'store/global_state'
import LocalStorage from 'localStorage'
import { serverAttr, localStoragePath } from 'confige'
import Axios from 'axios'

export const AUTHDATA_DB_ERROR = 'AUTHDATA_DB_ERROR'
export const AUTHDATA_JWT_NOT_EXIST = 'AUTHDATA_JWT_NOT_EXIST'
export const AUTHDATA_CORRECT = 'AUTHDATA_CORRECT'

export function boot (store) {
  let token = LocalStorage.getItem(localStoragePath.jwt)
  if (token !== '') {
    Axios({
      method: 'post',
      url: serverAttr.authentication,
      data: {
        jwt: token
      }
    })
      .then(response => {
        if (response.data.type === AUTHDATA_CORRECT) {
          store.dispatch(changeAuthorization(true, response.data.id, token))
        }
      })
  }
}
