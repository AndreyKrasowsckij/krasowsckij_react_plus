let registration = require('./filters/registration')
let authentication = require('./filters/authentication')
let login = require('./filters/login')
let users = require('./filters/users')
let image = require('./filters/image')
let info = require('./filters/info')
let newslist = require('./filters/newslist')
let news = require('./filters/news')

const setRoute = function (app, pool, upload) {
  app.post('/registration', (req, res) => {
    registration.filter(req.body, pool, res)
  })

  app.post('/login', (req, res) => {
    login.filter(req.body, pool, res)
  })

  app.post('/authentication', (req, res) => {
    authentication.filter(req.body, pool, res)
  })

  app.post('/users', (req, res) => {
    users.filter(req.body, pool, res)
  })

  app.post('/image', upload.single('image'), function (req, res, next) {
    image.filter(req.file, req.body, pool, res)
  })

  app.post('/info', (req, res) => {
    info.filter(req.body, pool, res)
  })

  app.post('/newslist', upload.single('image'), function (req, res, next) {
    newslist.filter(req.file, req.body, pool, res)
  })

  app.post('/news', (req, res) => {
    news.filter(req.body, pool, res)
  })
}
module.exports.setRoute = setRoute
