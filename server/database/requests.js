let addUser = (id, userName, jwt) => {
  return `INSERT INTO users (id, user_name, jwt, first_name, second_name, age, description, news, image) VALUES 
  (${id}, '${userName}', '${jwt}', '', '',0, '', ARRAY[]::integer[], '')`
}
module.exports.addUser = addUser

let getUserByLogin = (username) => {
  return `SELECT * FROM users WHERE user_name='${username}'`
}
module.exports.getUserByLogin = getUserByLogin

let getMaxId = () => {
  return `SELECT MAX(id) FROM users`
}
module.exports.getMaxId = getMaxId

let getUserByLoginJwt = (jwt) => {
  return `SELECT * FROM users WHERE jwt='${jwt}'`
}
module.exports.getUserByLoginJwt = getUserByLoginJwt

let getUserById = (id) => {
  return `SELECT * FROM users WHERE id=${id}`
}
module.exports.getUserById = getUserById

let addImage = (id, path) => {
  return `UPDATE users SET image='${path}' WHERE id=${id}`
}
module.exports.addImage = addImage

let addInfo = (id, firstName, secondName, age) => {
  return `UPDATE users SET first_name='${firstName}', second_name='${secondName}', age=${age}   WHERE id=${id}`
}
module.exports.addInfo = addInfo

let getNewsMaxId = () => {
  return `SELECT MAX(id) FROM news`
}
module.exports.getNewsMaxId = getNewsMaxId

let addNews = (id, lord, tags, thems, body, image) => {
  return `INSERT INTO news (id, lord, tags, thems, body, image) VALUES
   (${id}, ${lord}, ARRAY${tags}, ARRAY${thems}, '${body}', '${image}')`
}
module.exports.addNews = addNews

let getUserNews = (id) => {
  return `SELECT news FROM users where id=${id}`
}
module.exports.getUserNews = getUserNews

let updateUserNews = (id, userNews) => {
  return `UPDATE users SET news=ARRAY[${userNews}] WHERE id=${id}`
}
module.exports.updateUserNews = updateUserNews

let getAllNews = (id) => {
  return `SELECT * FROM news`
}
module.exports.getAllNews = getAllNews

let getNewsByLord = (id) => {
  return `SELECT * FROM news WHERE lord=${id}`
}
module.exports.getNewsByLord = getNewsByLord
