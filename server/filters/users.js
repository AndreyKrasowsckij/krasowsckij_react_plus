let sqlRequests = require('./../database/requests')
let answerType = require('./../actions/answer_type')

let filter = function (body, pool, res) {
  pool.query(sqlRequests.getUserById(body.id))
    .then(resUserById => {
      if (resUserById.rows.length === 1) {
        res.json({
          type: answerType.USRDATA_CORRECT,
          user_name: resUserById.rows[0].user_name,
          first_name: resUserById.rows[0].first_name,
          second_name: resUserById.rows[0].second_name,
          age: resUserById.rows[0].age,
          image: resUserById.rows[0].image
        })
      } else {
        res.json({
          type: answerType.USRDATA_LOGIN_NOT_EXIST
        })
      }
    })
    .catch(err => {
      res.json({
        type: answerType.USRDATA_DB_ERROR
      })
    })
}
module.exports.filter = filter
