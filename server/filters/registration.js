let sqlRequests = require('./../database/requests')
let answerType = require('./../actions/answer_type')
let JWT = require('jsonwebtoken')
let config = require('./../config')

let filter = function (body, pool, res) {
  pool.query(sqlRequests.getUserByLogin(body.username))
    .then(resUserByLogin => {
      // Проверка на совпадение логина
      if (resUserByLogin.rows.length === 0) {
        pool.query(sqlRequests.getMaxId())
          // Поиск наибольшего id
          .then(resMaxId => {
            let maxId = resMaxId.rows[0].max
            console.log(maxId)
            if (maxId === undefined || maxId < 0) {
              res.json({
                type: answerType.REGDATA_DB_ERROR
              })
            } else {
              // Генерация токена, добавление юзера в бд
              let token = JWT.sign({ password: body.password, login: body.username }, config.passPhrase)
              pool.query(sqlRequests.addUser(maxId + 1, body.username, token))
              // Отправка пользователю его токена, id и сообщения о успешной регистрации
                .then(resAddUser => {
                  res.json({
                    type: answerType.REGDATA_CORRECT,
                    jwt: token,
                    id: maxId + 1
                  })
                })
                .catch(err => {
                  res.json({
                    type: answerType.REGDATA_DB_ERROR
                  })
                })
            }
          })
      } else {
        res.json({
          type: answerType.REGDATA_LOGIN_EXIST
        })
      }
    })
    .catch(err => {
      res.json({
        type: answerType.REGDATA_DB_ERROR
      })
    })
}
module.exports.filter = filter
