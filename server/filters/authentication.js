let sqlRequests = require('./../database/requests')
let answerType = require('./../actions/answer_type')

let filter = function (body, pool, res) {
  pool.query(sqlRequests.getUserByLoginJwt(body.jwt))
    .then(resUserByJwt => {
      if (resUserByJwt.rows.length === 1) {
        res.json({
          type: answerType.AUTHDATA_CORRECT,
          id: resUserByJwt.rows[0].id
        })
      } else {
        res.json({
          type: answerType.AUTHDATA_JWT_NOT_EXIST
        })
      }
    })
    .catch(err => {
      res.json({
        type: answerType.AUTHDATA_DB_ERROR
      })
    })
}
module.exports.filter = filter
