let sqlRequests = require('./../database/requests')
let answerType = require('./../actions/answer_type')

let filter = function (body, pool, res) {
  let filterTags = body.filter_tags.split(' ')
  let filterThems = body.filter_thems.split(' ')
  let search = body.search

  let res_mas = []
  let request
  if (body.id === -1) {
    request = sqlRequests.getAllNews
  } else {
    request = sqlRequests.getNewsByLord
  }
  pool.query(request(body.id))
    .then(resAllNews => {
      for (let i = 0; i < resAllNews.rows.length; i++) {
        let flag = true

        console.log('filterTags.length', filterTags.length)
        if (body.filter_tags !== '') {
          for (let x = 0; x < filterTags.length; x++) {
            if (resAllNews.rows[i].tags.indexOf(filterTags[x]) === -1) {
              flag = false
            }
          }
        }

        console.log('filterThems.length', filterThems.length)
        if (body.filter_thems !== '') {
          for (let x = 0; x < filterThems.length; x++) {
            if (resAllNews.rows[i].thems.indexOf(filterThems[x]) === -1) {
              flag = false
            }
          }
        }

        console.log('searchh', search)
        if (search !== '') {
          if (resAllNews.rows[i].body.indexOf(search) === -1) {
            flag = false
          }
        }

        if (flag) {
          res_mas.push(resAllNews.rows[i])
        }
      }

      if (res_mas.length !== 0) {
        let allCount = allPages(res_mas, body.page_count)
        if (body.current_page < allCount) {
          let page_mas = []
          for (let x = 0; x < body.page_count; x++) {
            if (body.current_page * body.page_count + x < res_mas.length) {
              page_mas.push(res_mas[body.current_page * body.page_count + x])
            }
          }
          res.json({
            type: answerType.GET_NEWS_CORRECT,
            news: page_mas,
            all_count: allCount
          })
        } else {
          res.json({
            type: answerType.PAGE_ERROR
          })
        }
      } else {
        res.json({
          type: answerType.GET_NEWS_CORRECT,
          news: [],
          all_count: 0
        })
      }
    })
    .catch(error => {
      console.log(error)
      res.json({
        type: answerType.NEWS_DB_ERROR
      })
    })
}
module.exports.filter = filter

function allPages (mas, pageSize) {
  let res = (mas.length - (mas.length % pageSize)) / pageSize
  if (mas.length % pageSize !== 0) { res += 1 }
  return res
}
