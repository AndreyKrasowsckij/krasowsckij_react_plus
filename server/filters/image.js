let sqlRequests = require('./../database/requests')
let answerType = require('./../actions/answer_type')

let filter = function (file, body, pool, res) {
  pool.query(sqlRequests.getUserById(body.id))
    .then(resUserById => {
      if (resUserById.rows.length === 1) {
        pool.query(sqlRequests.addImage(body.id, file.filename))
          .then(resAddImage => {
            res.json({
              type: answerType.USRDATA_CORRECT,
              image: file.filename
            })
          })
          .catch(error => {
            res.json({
              type: answerType.USRDATA_DB_ERROR
            })
          })
      } else {
        res.json({
          type: answerType.USRDATA_LOGIN_NOT_EXIST
        })
      }
    })
    .catch(error => {
      res.json({
        type: answerType.USRDATA_DB_ERROR
      })
    })
}
module.exports.filter = filter
