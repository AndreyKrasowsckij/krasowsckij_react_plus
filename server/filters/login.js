let sqlRequests = require('./../database/requests')
let answerType = require('./../actions/answer_type')
let JWT = require('jsonwebtoken')
let config = require('./../config')

let filter = function (body, pool, res) {
  pool.query(sqlRequests.getUserByLogin(body.username))
    .then(resUserByLogin => {
      // Проверка на совпадение логина
      if (resUserByLogin.rows.length === 1) {
        if (body.password === JWT.decode(resUserByLogin.rows[0].jwt).password) {
          res.json({
            type: answerType.LOGDATA_CORRECT,
            jwt: resUserByLogin.rows[0].jwt,
            id: resUserByLogin.rows[0].id
          })
        } else {
          res.json({
            type: answerType.LOGDATA_PASSWORD_INCORRECT
          })
        }
      } else {
        res.json({
          type: answerType.LOGDATA_LOGIN_NOT_EXIST
        })
      }
    })
    .catch(err => {
      res.json({
        type: answerType.LOGDATA_DB_ERROR
      })
    })
}
module.exports.filter = filter
