let sqlRequests = require('./../database/requests')
let answerType = require('./../actions/answer_type')

let filter = function (file, body, pool, res) {
  pool.query(sqlRequests.getUserById(body.id))
    .then(resUserById => {
      if (resUserById.rows.length === 1) {

        pool.query(sqlRequests.getNewsMaxId())
          .then(resMaxId => {
            let maxId = resMaxId.rows[0].max
            let tags = funcPol(body.tags.split(' '))
            let thems = funcPol(body.thems.split(' '))
            pool.query(sqlRequests.addNews(maxId + 1, body.id, tags, thems, body.body, file.filename))
              .then(resAddNews => {
                pool.query(sqlRequests.getUserNews(body.id))
                  .then(resUserNews => {
                    let userNews = resUserNews.rows[0].news
                    console.log(userNews)
                    userNews.push(maxId + 1)
                    pool.query(sqlRequests.updateUserNews(body.id, userNews))
                      .then(resUpdateUserNews => {
                        res.json({
                          type: answerType.CORRECT_SAVE_NEWS
                        })
                      })
                      .catch(error => {
                        console.log(error)
                        res.json({
                          type: answerType.NEWS_DB_ERROR
                        })
                      })
                  })
                  .catch(error => {
                    console.log(error)
                    res.json({
                      type: answerType.NEWS_DB_ERROR
                    })
                  })
              })
              .catch(error => {
                console.log(error)
                res.json({
                  type: answerType.NEWS_DB_ERROR
                })
              })
          })
          .catch(error => {
            console.log(error)
            res.json({
              type: answerType.NEWS_DB_ERROR
            })
          })

      } else {
        res.json({
          type: answerType.USRDATA_LOGIN_NOT_EXIST
        })
      }
    })
    .catch(error => {
      console.log(error)
      res.json({
        type: answerType.USRDATA_DB_ERROR
      })
    })
}
module.exports.filter = filter

let funcPol = (mas) => {
  let res = '['
  for (let i = 0; i < mas.length; i++) {
    res +=  "'" + mas[i] + "'"
    if (i !== mas.length - 1) {
      res += ','
    } else {
      res += ']'
    }
  }
  return res
}
