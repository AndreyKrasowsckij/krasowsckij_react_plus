let sqlRequests = require('./../database/requests')
let answerType = require('./../actions/answer_type')

let filter = function (body, pool, res) {
  pool.query(sqlRequests.getUserById(body.id))
    .then(resUserById => {
      if (resUserById.rows.length === 1) {
        pool.query(sqlRequests.addInfo(body.id, body.first_name, body.second_name, body.age))
          .then(resAddInfo => {
            res.json({
              type: answerType.USRDATA_CORRECT,
              first_name: body.first_name,
              second_name: body.second_name,
              age: body.age
            })
          })
          .catch(err => {
            console.log(err)
            res.json({
              type: answerType.USRDATA_DB_ERROR,
              first_name: resUserById.rows[0].first_name,
              second_name: resUserById.rows[0].second_name,
              age: resUserById.rows[0].age
            })
          })
      } else {
        res.json({
          type: answerType.USRDATA_LOGIN_NOT_EXIST,
          first_name: resUserById.rows[0].first_name,
          second_name: resUserById.rows[0].second_name,
          age: resUserById.rows[0].age
        })
      }
    })
    .catch(err => {
      console.log(err)
      res.json({
        type: answerType.USRDATA_DB_ERROR,
        first_name: 'error',
        second_name: 'error',
        age: 0
      })
    })
}
module.exports.filter = filter
